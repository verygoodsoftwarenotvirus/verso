// +build wasm

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/verygoodsoftwarenotvirus/verso/html"
	"gitlab.com/verygoodsoftwarenotvirus/verso/html/components"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type personResponse struct {
	Page       uint     `json:"page"`
	PerPage    uint     `json:"per_page"`
	Total      uint     `json:"total"`
	TotalPages uint     `json:"total_page"`
	Data       []person `json:"data"`
}

type person struct {
	ID        uint64 `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Avatar    string `json:"avatar"`
}

func fetchPageOfPersons(page uint) []person {
	res, err := http.Get(fmt.Sprintf("https://reqres.in/api/users?page=%d", page))
	if err != nil {
		log.Fatal(err)
	}

	var pr personResponse

	if err := json.NewDecoder(res.Body).Decode(&pr); err != nil {
		log.Fatal(err)
	}

	return pr.Data
}

type dummyRestAPIProvider struct {
	currentPage uint
}

func (p *dummyRestAPIProvider) LastPage() interface{} {
	if p.currentPage != 0 {
		p.currentPage--
	}
	return fetchPageOfPersons(p.currentPage)
}

func (p *dummyRestAPIProvider) NextPage() interface{} {
	p.currentPage++
	return fetchPageOfPersons(p.currentPage)
}

//

func main() {
	fmt.Println("getting body")
	body := html.Body()

	// ignore this
	head := html.Head()
	cssLink := html.NewElement("link")
	jsv := cssLink.JSValue()
	jsv.Set("rel", "stylesheet")
	jsv.Set("type", "text/css")
	jsv.Set("href", "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.css")
	head.AppendChild(cssLink)
	// okay go on

	pp := &dummyRestAPIProvider{
		currentPage: 2,
	}

	fmt.Println("building table")
	data := fetchPageOfPersons(pp.currentPage)

	table, err := components.NewTableFromStructs("thing", data)
	if err != nil {
		log.Fatal(err)
	}

	table.SetPageProvider(pp)
	table.Render()
	table.HTMLTable.ClassList.Add("table")

	body.AppendChild(table)

	// ticker := time.NewTicker(3 * time.Second)
	ticker := time.NewTicker(time.Millisecond)
	for {
		select {
		case <-ticker.C:
			//
		}
	}
}
