// +build wasm

package main

import (
	"fmt"
	"time"

	"gitlab.com/verygoodsoftwarenotvirus/verso/html"
	"gitlab.com/verygoodsoftwarenotvirus/verso/router/fragment"
)

// ImageViewRenderer blah
type ImageViewRenderer struct{ img string }

// Render blah
func (i *ImageViewRenderer) Render() (string, error) {
	return fmt.Sprintf("<img src=%q>", i.img), nil
}

// NewImageViewRenderer blah
func NewImageViewRenderer(imgSrc string) router.ViewRenderer {
	return &ImageViewRenderer{
		img: imgSrc,
	}
}

// This function is special because I'm using wasm-server to test, and I can't easily edit the HTML that wasm-server serves
// but I can pretty easily add it here.
func buildPagePrerequisites() {
	ul := html.NewUnorderedList([]string{
		fmt.Sprintf(`<a href="#%s">%s</a>`, "/", "/"),
		fmt.Sprintf(`<a href="#%s">%s</a>`, "/page1", "/page1"),
		fmt.Sprintf(`<a href="#%s">%s</a>`, "/page2", "/page2"),
	})
	body := html.Body()

	d := html.NewDiv()
	d.SetID("view")

	body.AppendChild(ul)
	body.AppendChild(d)
}

func main() {
	fmt.Println("WASM running")
	buildPagePrerequisites()

	csr := router.NewClientSideRouter("view")
	// random images
	csr.AddRoute("/", NewImageViewRenderer("https://i.imgur.com/K5sxzxM.jpg"))
	csr.AddRoute("/page1", NewImageViewRenderer("https://i.imgur.com/A0j6Q39.jpg"))
	csr.AddRoute("/page2", NewImageViewRenderer("https://i.imgur.com/4e42WAg.jpg"))

	window := html.GetWindow()
	window.AddEventListener("hashchange", csr.RouteFunc())

	csr.Route()

	// suspend loop
	for {
		select {
		case <-time.NewTicker((1<<31 - 1) * time.Second).C:
			//
		}
	}
}
