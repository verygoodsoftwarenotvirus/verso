// +build wasm

package components

import (
	"errors"
	"fmt"
	"log"
	"reflect"
	"sort"
	"syscall/js"

	"gitlab.com/verygoodsoftwarenotvirus/verso/html"
)

// PageProvider is a mechanism for fetching the next page of items from a data source
type PageProvider interface {
	LastPage() interface{}
	NextPage() interface{}
}

type header struct {
	DisplayName   string
	ReferenceName string
}

// Table represents a dynamic table that can self-sort any simple structs
type Table struct {
	id string

	HTMLTable    *html.Table
	pageProvider PageProvider

	headers   []header
	headerMap map[string]header
	rows      []*html.TableRow

	sortBy  string
	sortAsc bool
}

// NewTableFromMap provides a new HTMLTable with an input of an array of maps
func NewTableFromMap(id string, input []map[string]string) *Table {
	t := html.NewTable()
	t.SetID(id)

	tbl := &Table{
		id:        id,
		HTMLTable: t,
		headers:   []header{},
		headerMap: map[string]header{},
		rows:      []*html.TableRow{},
	}

	for _, m := range input {
		for k := range m {
			if _, ok := tbl.headerMap[k]; !ok {
				h := header{DisplayName: k, ReferenceName: k}
				tbl.headerMap[k] = h
				tbl.headers = append(tbl.headers, h)
			}
		}
	}

	for _, m := range input {
		tbl.AddRow(m)
	}
	return tbl
}

func loadData(tbl *Table, input interface{}) error {
	tbl.headers = []header{}
	tbl.headerMap = map[string]header{}
	tbl.rows = []*html.TableRow{}

	if reflect.TypeOf(input).Kind() == reflect.Slice {

		tbl.determineHeaders(input)
		val := reflect.ValueOf(input)

		for i := 0; i < val.Len(); i++ {
			rowItem := val.Index(i)
			newRow := map[string]string{}
			for _, header := range tbl.headers {
				f := rowItem.FieldByName(header.ReferenceName)
				x := f.Interface()
				k := reflect.ValueOf(x).Kind()

				if k == reflect.Ptr {
					if !reflect.ValueOf(x).IsNil() {
						z := reflect.Indirect(f).Interface()
						newRow[header.DisplayName] = fmt.Sprintf("%v", z)
					}
				} else if k == reflect.Struct {
					// warn/panic here?
					continue
				} else {
					newRow[header.DisplayName] = fmt.Sprintf("%v", x)
				}

			}
			tbl.AddRow(newRow)
		}
	} else {
		return errors.New("type provided must be slice")
	}
	return nil
}

// NewTableFromStructs provides a new table given an array of simple structs
func NewTableFromStructs(id string, input interface{}) (*Table, error) {
	t := html.NewTable()
	t.SetID(id)

	tbl := &Table{
		id:        id,
		HTMLTable: t,
		headers:   []header{},
		headerMap: map[string]header{},
		rows:      []*html.TableRow{},
	}

	if err := loadData(tbl, input); err != nil {
		return nil, err
	}

	return tbl, nil
}

// SetPageProvider sets the page provider
func (t *Table) SetPageProvider(input PageProvider) {
	log.Println("SetPageProvider called")
	t.pageProvider = input
}

func (t *Table) determineHeaders(input interface{}) {
	typ := reflect.TypeOf(input).Elem()

	for i := 0; i < typ.NumField(); i++ {
		f := typ.Field(i)
		var fn = f.Name
		if v, ok := f.Tag.Lookup("header"); ok {
			fn = v
		}

		t.AddHeader(header{DisplayName: fn, ReferenceName: f.Name})
		if t.sortBy == "" {
			t.sortBy = f.Name
		}
	}
}

func (t Table) Len() int {
	return len(t.rows)
}

func (t Table) Swap(i, j int) {
	t.rows[i], t.rows[j] = t.rows[j], t.rows[i]
}

func (t Table) Less(i, j int) bool {
	sb := t.sortBy
	if sb == "" {
		for _, header := range t.headers {
			sb = header.DisplayName
			break
		}
	}

	if !t.sortAsc {
		return t.rows[i].GetCell(sb).Content > t.rows[j].GetCell(sb).Content
	}

	return t.rows[i].GetCell(sb).Content < t.rows[j].GetCell(sb).Content

}

func (t *Table) addRow(v reflect.Value) {
	newRow := map[string]string{}
	for _, header := range t.headers {
		f := v.FieldByName(header.ReferenceName)
		x := f.Interface()
		k := reflect.ValueOf(x).Kind()

		if k == reflect.Ptr {
			if !reflect.ValueOf(x).IsNil() {
				z := reflect.Indirect(f).Interface()
				newRow[header.DisplayName] = fmt.Sprintf("%v", z)
			}
		} else if k == reflect.Struct {
			continue
		} else {
			newRow[header.DisplayName] = fmt.Sprintf("%v", x)
		}

	}
	t.AddRow(newRow)
}

// AddHeader adds a header element (column) to the table
// TODO: the obvious, holy christ the name
func (t *Table) AddHeader(h header) {
	fmt.Println("AddHeader called")
	t.headers = append(t.headers, h)
	t.headerMap[h.DisplayName] = h

	header := t.HTMLTable.AddHeader(h.DisplayName)
	header.SetTextContent(h.DisplayName)
	header.OnClick(t.headerSort(h), false)
}

func (t *Table) headerSort(h header) func() {
	return func() {
		fmt.Printf("%q clicked\n", h.DisplayName)
		if t.sortBy != h.DisplayName {
			fmt.Printf("setting sortBy to %q\n", h.DisplayName)
			t.sortBy = h.DisplayName
		} else {
			t.sortAsc = !t.sortAsc
		}
		sort.Sort(t)

		t.Redraw()
	}
}

// AddRow adds a row of data to the table
func (t *Table) AddRow(data map[string]string) {
	fmt.Println("AddRow called")
	newRow := t.buildRow(data)
	t.rows = append(t.rows, newRow)
	t.HTMLTable.AddRow(data)
}

func (t *Table) buildRow(data map[string]string) *html.TableRow {
	cellData := []string{}
	for _, h := range t.headers {
		cellData = append(cellData, data[h.DisplayName])
	}

	var sh []string
	for _, h := range t.headers {
		sh = append(sh, h.DisplayName)
	}

	nr := html.NewTableRow(sh, data)
	return nr
}

// Render creates our outer table shell element so we can inject our header and row html in afterwards
func (t *Table) Render() {
	fmt.Println("Render called")
	t2 := html.NewTable()
	t2.SetID(t.id)

	for _, header := range t.headers {
		th := t2.AddHeader(header.DisplayName)
		th.OnClick(t.headerSort(header), false)
		t2.AddRawTableHeader(th)
	}

	for _, tr := range t.rows {
		t2.AddRawTableRow(tr)
	}

	if t.pageProvider != nil {
		prevButton := html.NewButton("<")
		prevButton.OnClick(func() {
			data := t.pageProvider.LastPage()
			if err := loadData(t, data); err != nil {
				log.Println("error loading data")
			}
			t.Redraw()
		}, false)

		nextButton := html.NewButton(">")
		nextButton.OnClick(func() {
			data := t.pageProvider.NextPage()
			if err := loadData(t, data); err != nil {
				log.Println("error loading data")
			}
			t.Redraw()
		}, false)

		t2.AppendToFooter(prevButton.JSValue())
		t2.AppendToFooter(nextButton.JSValue())
	}

	t2.Render()
	oldClassList := t.HTMLTable.ClassList.String()
	if t.HTMLTable.ClassList != nil && oldClassList != "" {
		t2.ClassList.Add(oldClassList)
	}
	t.HTMLTable = t2
}

// Redraw redraws the table with the current contents of the table
func (t *Table) Redraw() {
	fmt.Println("Redraw called")

	t.Render()

	body := html.Body()
	body.RemoveChild(html.GetDocument().GetElementByID(t.id))
	body.AppendChild(t.HTMLTable)
}

// JSValue retuns the inner value of the table
func (t *Table) JSValue() js.Value {
	return t.HTMLTable.JSValue()
}
