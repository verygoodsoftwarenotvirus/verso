.PHONY: table-demo

table-demo:
	wasm-server -main=demos/table-demo

button-demo:
	wasm-server -main=demos/button_alert

fragment-routing-demo:
	wasm-server -main=demos/fragment_routing


## Go-specific prerequisite stuff
.PHONY: vendor-clean
vendor-clean:
	rm -rf vendor go.{mod,sum}

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

.PHONY: revendor
revendor: vendor-clean vendor

.PHONY: add-version
add-version:
ifdef branch
	git tag v$(branch) $(branch)
	git push origin :refs/tags/$(branch)
	git push --tags;
else
	@echo "branch undefined"
endif
