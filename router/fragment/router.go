// +build wasm

package router

import (
	"errors"
	"strings"
	"syscall/js"

	"gitlab.com/verygoodsoftwarenotvirus/verso/html"
)

// ViewRenderer renders views
type ViewRenderer interface {
	Render() (string, error)
}

// ClientSideRouter manages view renderers
type ClientSideRouter struct {
	hostElement js.Value
	routes      map[string]ViewRenderer
}

// NewClientSideRouter instantiates a new ClientSideRouter
func NewClientSideRouter(hostDivID string) *ClientSideRouter {
	return &ClientSideRouter{
		routes:      make(map[string]ViewRenderer),
		hostElement: html.GetDocument().GetElementByID("view").JSValue(),
	}
}

// RouteFunc returns a jsFunc that should be assigned to hashchange events
func (r *ClientSideRouter) RouteFunc() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		return r.Route()
	})
}

// Route is our main function which determines what page we're on, what that page should show, and reconciles the difference
func (r *ClientSideRouter) Route() error {
	var url = "/"
	fullHash := html.GetLocation().Hash()
	urlParts := strings.Split(fullHash, "#")
	if len(urlParts) > 1 {
		url = urlParts[1]
	}

	route, ok := r.routes[url]
	if !ok {
		return errors.New("blah")
	}

	view, err := route.Render()
	if err != nil {
		return err
	}

	r.hostElement.Set("innerHTML", view)

	return nil
}

// AddRoute adds a new route to the router
func (r *ClientSideRouter) AddRoute(path string, vr ViewRenderer) {
	r.routes[path] = vr
}
